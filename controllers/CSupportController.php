<?php
/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 18.07.16
 * Time: 10:56
 */
namespace controllers;

use system\AController;
use system\CPagination;
use system\CVarDump;
use system\CView;
use system\MonCms;
use models\CSupportModel;
use system\widgets\CWidgetCommentList;

class CSupportController extends AController
{
    public $supportModel = null;

    /**
     * ������������� ������
     * @return CSupportModel|null
     */
    public function loadModel()
    {
        if ($this->supportModel === null)
            $this->supportModel = new CSupportModel(MonCms::$db, MonCms::$config);

        return $this->supportModel;
    }

    public function checkRights()
    {
        $rights = [];

        if (check_group('supportPlanEdit'))
            $rights['planEdit'] = true;

        if (check_group('supportPlanView'))
            $rights['planView'] = true;


        return $rights;
    }

    /**
     * ������ ��������� ��������
     * @return string
     */
    public function actionIndex()
    {
        global $err_msg;
        $rights = $this->checkRights();
        $support = $this->loadModel();
        $team = $support->getTeam();
        $driver = $support->getDriver();
        $overtimeStatus = $support->getovertimeStatus();
        $workStatus = $support->getWorkStatus();
        if (isset($_GET['page']))
            $support->setAttribute('page', $_GET['page']);

        $driverFilter = 1;
        $countAllPlan = $support->countAllPlan();
        $support->pageLen = MonCms::$config['page_count'];
        $support->start = ($support->page - 1) * $support->pageLen;
        $pages = new CPagination(ceil($countAllPlan / $support->pageLen));


        if (isset($_GET['filter']))
        {
            if ($_GET['filter']['driverFilter'])
            {
                $_GET['filter']['driverFilter'] = '1';
                $_GET['filter']['driverId'] = null;
                $driverFilter = 1;
            }
            else
            {
                $_GET['filter']['driverFilter'] = '0';
                $driverFilter = 0;
            }
            //��������� ������ ��� csv
            $csv = './index.php?mod=support&act=csv';
            foreach ($_GET['filter'] as $key => $val)
            {
                $csv .= '&filter['.$key.']='.$val;
            }

            $support->setAttributes($_GET['filter']);
            if ($support->validate('filter'))
            {
                $support->isFilter = 1;
                $filter = $_GET['filter'];
                $supportPlan = $support->getAllplan();
            }
            else
            {
                $err_msg = $support->getErrors()->getErrorListString();
            }
        }
        $supportPlan = $support->getAllplan();

        return $this->render('supportPlan/supportPlanTable.tpl', ['supportPlan'    => $supportPlan,
                                                                  'team'           => $team,
                                                                  'driver'         => $driver,
                                                                  'overtimeStatus' => $overtimeStatus,
                                                                  'workStatus'     => $workStatus,
                                                                  'pages'          => $pages,
                                                                  'filter'         => $filter,
                                                                  'filterContract' => $_GET['filterContract'],
                                                                  'filterAddress'  => $_GET['filterAddress'],
                                                                  'filterAuthor'   => $_GET['filterAuthor'],
                                                                  'rights'         => $rights,
                                                                  'csv'            => $csv,
                                                                  'driverFilter'   => $driverFilter]);
    }

    /**
     * ���� ���������� ������ ������
     */
    public function actionAddPlan()
    {
        $support = $this->loadModel();

        global $err_msg;
        if (isset($_POST['supportPlan']))
        {
            $support->setAttributes($_POST['supportPlan']);
            if ($support->validate('supportPlan'))
            {
                $support->description = $_POST['description'];
                $support->addPlan();
                header('Location: ./index.php?mod=support');
            }
            else
            {
                $err_msg = $support->getErrors()->getErrorListString();
            }
        }
    }

    /**
     * ���� ��������� ������ �� ������ ��� �������������� �� ��� id
     */
    public function actionGetPlanById()
    {
        $support = $this->loadModel();

        global $err_msg;
        $rights = $this->checkRights();
        if (isset($_GET['planId']))
        {
            $support->setAttribute('planId', $_GET['planId']);
            if ($support->validate('planId'))
            {
                $planById = $support->getAllPlan();
                $team = $support->getTeam();
                $driver = $support->getDriver();
                $overtimeStatus = $support->getOvertimeStatus();
                $workStatus = $support->getWorkStatus();
                return $this->render('supportPlan/supportPlanEdit.tpl', ['team'               => $team,
                                                                         'driver'             => $driver,
                                                                         'overtimeStatus'     => $overtimeStatus,
                                                                         'workStatus'         => $workStatus,
                                                                         'planById'           => $planById,
                                                                         'rights'             => $rights,
                                                                         'CWidgetCommentList' => new CWidgetCommentList(CView::$template, $support->getPlanComment()),]);
            }
            else
            {
                $err_msg = $support->getErrors()->getErrorListString();
            }
        }
    }

    /**
     * ���� �������� ����������������� ������ � ����
     */
    public function actionEditPlan()
    {
        $support = $this->loadModel();
        global $err_msg;

        if (isset($_POST['supportPlanEdit']))
        {
            $support->setAttributes($_POST['supportPlanEdit']);
            if ($support->validate('supportPlanEdit'))
            {

                $support->description = $_POST['description'];
                $support->contractNo = $_POST['contractNo'];
                $support->address = $_POST['editAddress'];
                $support->editPlan();
                header('Location: ./index.php?mod=support&act=getPlanById&planId='.$_POST['supportPlanEdit']['planId']);
            }
            else
            {
                $err_msg = $support->getErrors()->getErrorListString();
            }
        }
    }

    /**
     * ���� ��������� ������ �� �������� ��� ������� ���������� � ��������
     */
    public function actionGetTeamForEdit()
    {
        $support = $this->loadModel();
        $team = $support->getTeam();
        echo json_encode(iconv_deep('windows-1251', 'utf-8', $team));
        exit;
    }

    /**
     * ���� ���������� ����� �������
     */
    public function actionAddNewTeam()
    {
        $support = $this->loadModel();
        global $err_msg;

        if (isset($_POST['newTeam']))
        {
            $support->newTeamName = iconv_deep('utf-8', 'windows-1251', $_POST['newTeam']);
            $support->addNewTeam();
            echo json_encode(1);
            exit;
        }
    }

    /**
     * ���� �������� �������� � ���
     */
    public function actionAddTeamInBan()
    {
        $support = $this->loadModel();
        global $err_msg;

        if (isset($_POST['teamId']))
        {
            $support->setAttribute('teamId', $_POST['teamId']);
            if ($support->validate('teamId'))
            {
                $support->addTeamInBan();
                echo json_encode(1);
                exit;
            }
            else
            {
                $err_msg = $support->getErrors()->getErrorListString();
            }
        }
    }

    public function actionAddComment()
    {
        $support = $this->loadModel();
        global $err_msg;

        if (isset($_POST['comment']))
        {
            $support->comment = $_POST['comment'];
            $support->setAttribute('planId', $_POST['commentPlanId']);
            if ($support->validate('planId'))
            {
                $support->addComment();
                header('Location: ./index.php?mod=support&act=getPlanById&planId='.$_POST['commentPlanId']);
            }
            else
            {
                $err_msg = $support->getErrors()->getErrorListString();
            }
        }
    }

    public function actionCsv()
    {
        $support = $this->loadModel();
        global $err_msg;

        $support->isCsv = 1;
        if (isset($_GET['filter']))
        {

            $support->isFilter = 1;
            $support->setAttributes($_GET['filter']);
            if ($support->validate('filter'))
            {
                $supportPlan = $support->getAllplan();
                header('Content-type: text/csv');
                header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Disposition: attachment; filename="schema_'.date('Y.m.d').'.csv"');

                echo '#;';
                echo '� ��������;';
                echo '� ������;';
                echo '������;';
                echo '�����;';
                echo '�����;';
                echo '������ ����������;';
                echo '�����������;';
                echo '������;';
                echo '�������;';
                echo '��������;';
                echo "\n";

                $i = 1;
                foreach ($supportPlan as $data)
                {
                    echo $i.';';
                    echo $data['contractNo'].';';
                    echo $data['ticketId'].';';
                    echo str_replace('&quot;', '"', $data['clientName']).';';
                    echo $data['city'].','.$data['street'].','.$data['house'].';';
                    echo $data['planDatetime'].';';
                    echo $data['statusDescription'].';';
                    echo $data['overtimeDescription'].';';
                    echo $data['fullname'].';';
                    echo $data['teamName'].';';
                    echo $data['driverName'].';';
                    echo "\n";
                    $i++;
                }
                exit;
            }
            else
            {
                $err_msg = $support->getErrors()->getErrorListString();
            }
        }
    }

}