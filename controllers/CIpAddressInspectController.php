<?php
namespace controllers;
use models\CIpAddressInspectModel;
use system\AController;
use system\CVarDump;
use system\MonCms;
require_once APPLICATION_DIR.'/php-core/includes/functions.php';

/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 21.02.17
 * Time: 12:49
 */

class CIpAddressInspectController extends AController
{

    public $model = null;

    /**
     * ������������� ������
     */
    public function loadModel()
    {
        if ($this->model === null)
            $this->model = new CIpAddressInspectModel(MonCms::$db, MonCms::$config);

        return $this->model;
    }

    public function checkAccess()
    {
        return check_group('ipAddressInspect') ? true : false;
    }

    public function actionIndex()
    {
        $model = $this->loadModel();
        $ip = $model->getIp()['ip'];
        $sumBusyIp = $model->getIp()['sumBusyIp'];
        $allIp = $model->getIp()['allIp'];
        $sumFreeIp = $allIp - $sumBusyIp;
        $diagramIp = $model->getDiagramIp();
        $totalIp = $model->getDiagramTotalIp();
        $networkIp = $model->countIp()['networkIp'];
        return $this->render('ipAddressInspect/ipAddressInspect.tpl', ['ip'         => $ip,
                                                                       'diagramIp'  => implode(',', $diagramIp),
                                                                       'totalIp'    => implode(',', $totalIp),
                                                                       'sumFreeIp'  => $sumFreeIp,
                                                                       'sumBusyIp'  => $sumBusyIp,
                                                                       'allIp'      => $allIp,
                                                                       'networkIp'  => $networkIp]);

    }


}