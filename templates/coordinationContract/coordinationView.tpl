<script>
    $(document).ready(function () {
        $('#followerUser').autocomplete(
        {
            serviceUrl: '../modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 1, // ����������� ����� ������� ��� ������������ ��������������
            delimiter: /(,|;)\s*/, // ����������� ��� ���������� ��������, ������ ��� ���������� ���������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            noCache: true,
            deferRequestBy: 300, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'users_list'},//, parent: val}, // �������������� ���������
            formatResult: userFormatResult,
            onSelect: function (data)
            {
                $('#followerUserId').val(data.data);
            }
        });

        $('#addFollower').on('click', function()
        {
            var coordinationId = '{{ coordination.id }}';
            var followerUserId =  $('#followerUserId').val();
            var contractNo =  $('#contractNo').val();
            $.ajax
            ({
                url: './index.php?mod=coordinationContract&act=addFollower',
                type: 'POST',
                data: {coordinationId:coordinationId,followerUserId: followerUserId ,contractNo: contractNo},
                dataType: 'json',
                success: function (response)
                {
                    location.reload();
                }
            });

            return false;
        });

        /*$('#commentText').keydown(function(e){
            if(e.keyCode == 8)
            {
                $('#colChar').text("��������: " + (1000 - (($('#commentText').val().length) + 1)) + " ��������");
            }
            else
            {
                $('#colChar').text("��������: " + (1000 - ($('#commentText').val().length)) + " ��������");
            }
        });*/

        $('#uploadFileForm').on('submit', function (e)
        {
            e.preventDefault();
            var data = new FormData($(this).get(0));

            $.ajax
                ({
                    url: $(this).attr('action'),
                    type: $(this).attr('method'),
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response)
                    {
                        if(response != 0)
                        {
                            location.href = "./index.php?mod=coordinationContract&coordinationId=" +  response;
                        }
                        else
                        {
                            send_error('','�������� ��� �����');
                        }
                    }
                });
        });

    });

    function coordinateFile(coordinationId, fileId)
    {
        $.ajax
            ({
                url: './index.php?mod=coordinationContract&act=coordinateFile',
                type: 'POST',
                data: {coordinationId:coordinationId,fileId: fileId},
                dataType: 'json',
                success: function (response)
                {
                    location.reload();
                }
            });
        return false;
    }

    function addComment()
    {
        var textLenght = $('#commentText').val().length;
        if(textLenght === 0)
        {
            send_error('','����������� �� ����� ���� ������');

        }
        else if(textLenght > 1000)
        {
            send_error('','����� ������ �� ������ ��������� 1000 ��������');
        }
        else
        {
            $('#addCommentForm').submit();

        }
    }

    function answerComment(commentId,fullname,addDate)
    {
        $('#commentId').val(commentId);
        $('#answerCommentUserName').val(fullname);
        $('#answerCommentAddDate').val(addDate);
        $('#parentId').val(commentId);
        $('#addComment').modal('show');
    }

</script>
<div class=row-fluid>
    <div class="alert alert-info"><h4>����� ����������</h4></div>
    <div class=row-fluid>
        <form id=coordinationForm action="./index.php?mod=coordinationContract&act=saveCoordination" method=post>
            <div class="span6">
                <table class="table table-bordered">
                        <tr>
                            <td>
                                ��������� ������������
                            </td>
                            <td>
                                {{ coordination.fullname }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ���� ��������
                            </td>
                            <td>
                                {{ coordination.addDate }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ������
                            </td>
                            <td>
                                {{ coordination.clientName | raw }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                �������
                            </td>
                            <td>
                                {{ coordination.contractNo }}
                                <input type=hidden id="contractNo" name="contractNo" value="{{ coordination.contractNo }}">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ��� ���������
                            </td>
                            <td>
                                {% if currentUser == coordination.userId %}
                                <select name=saveCoordination[typeDocumentId]>
                                    {% for type in documentType %}
                                        {% if coordination.typeDocumentId == type.id %}
                                            <option value={{ type.id }} selected>{{ type.documentType }}</option>
                                        {% else %}
                                            <option value={{ type.id }}>{{ type.documentType }}</option>
                                        {% endif %}
                                    {% endfor %}
                                </select>
                                    {% for type in documentType %}
                                        {% if coordination.typeDocumentId == type.id %}
                                            <input type=hidden name="saveCoordination[lastTypeDocumentId]" value="{{ type.id }}">
                                            <input type=hidden name="lastTypeDocumentDescription" value="{{ type.documentType }}">
                                        {% endif %}
                                    {% endfor %}
                                {% else %}
                                    {% for type in documentType %}
                                        {% if coordination.typeDocumentId == type.id %}
                                            {{ type.documentType }}
                                        {% endif %}
                                    {% endfor %}
                                {% endif %}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ������
                            </td>
                            <td>
                                {% if currentUser == coordination.userId %}
                                <select name=saveCoordination[statusId]>
                                    {% for status in coordinationStatus %}
                                        {% if coordination.statusId == status.id %}
                                            <option value={{ status.id }} selected>{{ status.status }}</option>
                                        {% else %}
                                            <option value={{ status.id }}>{{ status.status }}</option>
                                        {% endif %}
                                    {% endfor %}
                                </select>
                                    {% for status in coordinationStatus %}
                                        {% if coordination.statusId == status.id %}
                                            <input type=hidden name="saveCoordination[lastStatusId]" value="{{ status.id }}">
                                            <input type=hidden name="lastStatusDescription" value="{{ status.status }}">
                                        {% endif %}
                                    {% endfor %}
                                {% else %}
                                    {% for status in coordinationStatus %}
                                        {% if coordination.statusId == status.id %}
                                            {{ status.status }}
                                        {% endif %}
                                    {% endfor %}
                                {% endif %}
                            </td>
                        </tr>
                        <tr>
                             <td>
                                ��������� ������������<br/>
                            </td>
                            <td>
                                {% if coordination.userId == currentUser %}
                                    <div class="span12 input-append indent-bottom">
                                        <input type="text" class="span11" name="followUser" id="followerUser" value="" placeholder="�������� ���������"/>
                                        <input type="hidden" name="followerUserId" id="followerUserId" value="" />
                                        <a class="btn" id="addFollower" title="�������� ������ ��������� ������������"><i class="ico_sprite ico_add"></i></a>

                                    </div>
                                {% endif %}

                                {% for follower in coordinationFollower %}
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <a href="./index.php?mod=employees&act=view&userId={{ follower['userId'] }}">{{ follower['fullname'] }}</a>
                                            (���. {{ follower['phone'] }})
                                            {% if follower.coordinationFileStatus == 1%}
                                                (<a style="color: #990000; text-decoration: none;">����������</a>)
                                            {% else %}
                                                (<a style="color: #0e0e0e; text-decoration: none;">�� ����������</a>)
                                            {% endif %}

                                            {% if coordination.userId == currentUser and follower.coordinationFileStatus != 1 %}
                                                <div class="span1 pull-right">
                                                    <a class="btn btn-mini " href="./index.php?mod=coordinationContract&act=deleteFollower&coordinationId={{ coordination.id }}&followerUserId={{ follower['userId'] }}&contractNo={{ coordination.contractNo }}">
                                                        <i class="ico_sprite ico_cross" style="margin-bottom: 5px;"></i>
                                                    </a>
                                                </div>
                                            {% endif %}
                                        </div>

                                    </div>
                                {% endfor %}
                            </td>
                        </tr>

                        <tr>
                            <td colspan=2>
                                {% if coordination.userId == currentUser %}
                                    <input class="pull-left btn btn-info" type="submit"  title="���������� ��������� ���� ��������� � �������" value=���������> &nbsp;&nbsp;
                                {% endif %}
                                <a class="btn btn-info" href="./index.php?mod=client&act=view&id={{ clientId }}">��������� � ��������</a>
                            </td>
                        </tr>
                    <input type=hidden name="saveCoordination[coordinationId]" value="{{ coordination.id }}">
                </table>
            </div>
        </form>
    </div>
    {% if countFollower != null %}
    <div class="alert alert-info"><h4>�����</h4></div>
    <div class=row-fluid>
        {% if not rights.coordinationView %}
            <a  href="#modalAddDocument" data-toggle="modal" class="btn"><i class="ico_sprite ico_plus-button"></i> ���������� ����</a>
        {% endif %}
        <br/>
        <br/>
        <table class="table table-bordered">
            <tr style="background-color:#DBE6F7;">
                <td class="span2">��������</td>
                <td class="span2">����</td>
                <td class="span6">����</td>
                <td class="span3">��� �������</td>
                <td class="span1">������������</td>
            </tr>
            {% for file in coordinationFile %}
                <tr>
                    <td>{{ file.description }}</td>
                    <td>{{ file.addDate }}</td>
                    <td>
                        <a href="./upload/clientCoordination/{{ coordination.id }}/{{ file.fileName }}" style="color:blue;text-decoration:underline;" target="_blank">
                            {{ file.fileName }}
                        </a>
                    </td>
                    <td>{{ file.fullname }}</td>

                    <td class="center">
                        {% if loop.first %}
                            {% for follower in coordinationFollower %}
                                {% if file.id == follower.coordinationFileId and follower.coordinationFileStatus == 0 and follower.userId == currentUser %}
                                    <a class="btn btn-success btn-xs" onclick="coordinateFile('{{ coordination.id }}','{{ file.id }}');return false;">�����������</a>
                                {% elseif  file.id == follower.coordinationFileId and follower.coordinationFileStatus == 1 and follower.userId == currentUser %}
                                    <a class="btn btn-success btn-xs disabled">�����������</a>
                                {% endif %}
                            {% endfor %}
                        {% endif %}
                    </td>
                </tr>
            {% endfor %}
        </table>
    </div>
    <div class="alert alert-info"><h4>�����������</h4></div>

    <a class="btn indent-bottom" data-toggle="modal" href="#addComment">
        <i class="ico_sprite ico_add"></i>
        �������� �����������
    </a>

    <div class=row-fluid>
        <div style="margin: 20px 0 20px 0; overflow: auto; width: auto; max-height: 300px;">
        {% for comment in coordinationComment %}
            <table class="table table-bordered" id="comment_{{ comment.id }}" >
                <thead >
                <th colspan="2" style="text-align:left; ">
                        �������(-�): <strong>{{ comment.fullname }}</strong>
                        <span style="font-size:0.8em; color: grey;">
                            ({{ comment.department }}, {{ comment.position }})
                            <a onclick="answerComment('{{ comment.id }}','{{ comment.fullname }}','{{ comment.addDate }}')" style="cursor: pointer;">��������</a>
                        </span>
                        <div style="float:right; color: grey;">{{ comment.addDate }}</div>
                </th>
                </thead>
                <tr>
                    <td style="width:230px;text-align:center;">
                        <div style="float:left; max-width:90px">
                            <img alt="" src="./upload/photo/w90_{{ comment.photo }}"/>
                        </div>
                        <div style="float:right;">
                            <ul style="text-align:left; padding-top:10px;"  class="unstyled">
                                <li>ICQ: {{ comment.icq }}</li>
                                <li>���: {{ comment.phone }}</li>
                                <li>���: {{ comment.mobile | raw }}</li>
                            </ul>
                        </div>
                    </td>
                    <td id="">
                        {{ comment.comment | raw  }}
                    </td>
                </tr>
            </table>
            </table>
        {% endfor %}
        </div>
    </div>
    {% endif %}
</div>


<div id="addComment" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="commentLabel" aria-hidden="true">
    <form id="addCommentForm" name="createComment" action="./index.php?mod=coordinationContract&act=addComment" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="commentLabel">���������� �����������</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <!--<div>
                    <span id="colChar" style="color: #8c8c8c;">��������: 1000 ��������</span>
                </div>-->
                <textarea class="span12" rows="7" id="commentText" name="commentText" placeholder="������� ����� ����������� �� ����� 1000 ��������"></textarea>
                <input type="hidden"  name="comment[coordinationId]" value="{{ coordination.id }}">
                <input type="hidden" id="commentId" name="comment[commentId]" value="0">
                <input type="hidden" id="parentId"  name="comment[parentId]" value="0">
                <input type="hidden" id="answerCommentText"  name="answer[answerCommentText]" value="">
                <input type="hidden" id="answerCommentUserName"  name="answer[answerCommentUserName]" value="">
                <input type="hidden" id="answerCommentAddDate"  name="answer[answerCommentAddDate]" value="">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="addComment();return false;">�����������</button>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">��������</button>
        </div>
    </form>
</div>
<!--action="./index.php?mod=coordinationContract&act=file" onSubmit="return false;"-->
<div id="modalAddDocument" class="modal hide fade" style="width: auto; margin-left: -14%;"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="uploadFileForm" method="post" action="./index.php?mod=coordinationContract&act=file" name="uploadFileForm" enctype="multipart/form-data" style="height: 130px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="myModalLabel">�������� ��������</h3>
        </div>
        <div class="modal-body">
            <div class="fileupload-buttonbar pull-right row-fluid">
                <span class="btn btn-mini btn-success fileinput-button pull-right" style="height: 26px;">
                        <i class="icon-plus icon-white" style="margin-bottom: -11px; margin-top: 2px;"></i>
                        <div style="margin-bottom: 5px; margin-top: 2px; margin-left: 3px; float: right;"><i style="vertical-align: middle;" >������� ����...</i></div>
                        <input type="file" name="file">
                </span>
                 <input type="text"  name="uploadFileCoordinationDescription" placeholder="������� �������� �����..." value="">
                <input type="hidden" name="coordinationFile[coordinationId]" value="{{ coordination.id }}">
                <input type="hidden" name="coordinationFile[userId]" value="{{ currentUser }}">
                <input type="hidden" name="coordinationFile[brandId]" value="{{ coordination.brandId }}">
                <input type="hidden" name="coordinationFile[clientId]" value="{{ coordination.clientId }}">
                <input type="hidden" name="contractNo" value="{{ coordination.contractNo }}">
            </div>
            <div style="padding-top: 40px;">�������������� �������: doc, docx, pdf</div>
        </div>
        <div class="modal-footer">
            <input id="submitFile" type="submit" class="btn btn-success" value="���������" />
            <input type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" value="������" />
        </div>
    </form>
</div>