<script>
    $(document).ready(function ()
    {
        $(function()
        {
            $("#planTime").timepicker();
        });
    });

    function addPlan()
    {
        if($('#ticketId').val().match(/[0-9]{6}[\/][0-9]{2}/) && $('#addContractNo').val() != '' && $('#planDate').val() != '' && $('#planTime').val() != '')
        {
            $('#createPlanForm').submit();
        }
        else
        {
            send_error('������','�������� ������ ������� ������');
        }
    }
</script>
<!--������� ��� ���������� ������-->
<div id="createPlan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <form name="createPlanForm" id="createPlanForm" action="./index.php?mod=support&act=addPlan" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="myModalLabel">������������ ������</h3>
        </div>

        <div class="modal-body" style="min-height: 480px;">
            <div class="row-fluid">
                <table class="table table-bordered">
                    <tr>
                        <td>
                            ����� ��������
                        </td>
                        <td>
                            <input type="text" id="addContractNo"  required/>
                            <input type="hidden" id="addOrderId"  name="supportPlan[orderId]" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ����� ������
                        </td>
                        <td>
                            <input type="text" id="ticketId" name="supportPlan[ticketId]"  placeholder="123456/16" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            �����
                        </td>
                        <td>
                            <input type="text" id="addAddress" class="span12" required/>
                            <input type="hidden" id="addHouseId" name="supportPlan[houseId]"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ���� � ����� ������
                        </td>
                        <td>
                            <div data-date-format="yyyy-mm-dd" data-date class="input-append date dp span6">
                                <input type="text" class="span8" id="planDate"  name="supportPlan[planDate]" size="16" value="" required="required">
                                <span class="add-on"><i class="icon-calendar"></i></span>
                                <span class="add-on"><i class="icon-trash"></i></span>
                            </div>
                            <div class="span3">
                                <input id="planTime" class="span9" type="text"  name="supportPlan[planTime]" size="10"  value="" required="required">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ������ ����������
                        </td>
                        <td>
                            <select name="supportPlan[workStatusId]" class="span11" required>
                                {% for ws in workStatus %}
                                    <option value="{{ ws.statusId }}">{{ ws.description }}</option>
                                {% endfor %}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ������������� �������
                        </td>
                        <td>
                            <select name="supportPlan[teamId]" class="span11" required>
                                {% for e in team %}
                                    <option value="{{ e.teamId }}">{{ e.team }}</option>
                                {% endfor %}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ��������
                        </td>
                        <td>
                            <select name="supportPlan[driverId]" class="span11">
                                {% for d in driver %}
                                    <option value="{{ d.userId }}">{{ d.driverName }}</option>
                                {% endfor %}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            �����������
                        </td>
                        <td>
                            <select name="supportPlan[overtimeStatusId]" class="span11" required>
                                {% for c in overtimeStatus %}
                                    <option value="{{ c.overtimeStatusId }}">{{ c.description }}</option>
                                {% endfor %}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ������� ��������
                        </td>
                        <td>
                            <textarea name="description" class="span12" rows="4" style="resize: none"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="addPlan();return false;">�����������</button>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">��������</button>
        </div>
    </form>
</div>