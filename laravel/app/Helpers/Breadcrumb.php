<?php

namespace App\Helpers;

/**
 * Class Breadcrumb
 * Класс для создания навигационного маршрута.
 *
 * @package App\Helpers
 */
class Breadcrumb
{
    protected static $breadcrumbs = [];
    public static $separator = ' -> ';

    public function __construct()
    {

    }

    /**
     * Добавить навигационный маршрут
     *
     * @param string $name Название
     * @param string $url  Ссылка
     *
     * @throws \Exception
     */
    public static function push($name = null, $url = null)
    {
        if ($name === null or $name == '')
            throw new \Exception('Параметр $name не может быть пустым!');

        self::$breadcrumbs[] = ['name' => $name, 'url' => $url];
    }

    /**
     * Вывести навигационный маршрут
     * Используется Bootstrap 3
     * @return string
     */
    public static function render()
    {
        $output = '<ol class="breadcrumb">';
        foreach (self::$breadcrumbs as $value)
        {
            $output .= ($value['url'] !== null) ? '<li><a href="'.url($value['url']).'">'.$value['name'].'</a></li>' :
                '<li class="active">'.$value['name'].'</li>';
        }
        $output .= '</ol>';

        //return implode(self::$separator, $output);
        return $output;
    }

    /**
     * Очистить навигационный маршрут
     */
    public static function clear()
    {
        self::$breadcrumbs = [];
    }
}
