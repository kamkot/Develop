<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

class FeedbackFile extends Model
{
    public $timestamps = false;
    protected $table = 'feedback_file';

    public function fileFeedback()
    {
        return $this->belongsTo('App\Feedback\Feedback','feedback_id');
    }
}
