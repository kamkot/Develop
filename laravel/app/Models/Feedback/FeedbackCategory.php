<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

class FeedbackCategory extends Model
{
    public $timestamps = false;
    protected $table = 'feedback_category';

}
