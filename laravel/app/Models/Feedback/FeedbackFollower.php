<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

class FeedbackFollower extends Model
{
    public $timestamps = false;
    protected $table = 'feedback_follower';

}
