<?php

namespace App\Http\Requests\Feedback;

use Illuminate\Foundation\Http\FormRequest;

class AddFeedback extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'departmentId' => 'required|exists:feedback_department_category,department_id',
            'categoryId'   => 'required|exists:feedback_category,id',
            'priority'     => 'required|in:low,middle,high',
            'title'        => 'required|max:120',
            'description'  => 'required|max:2000',
        ];
    }

    public function messages()
    {
        return [
            'departmentId.required' => 'Необходимо указать отдел',
            'departmentId.exists'   => 'Неверный ID отдела',
            'categoryId.required'   => 'Необходимо тип задачи',
            'categoryId.exists'     => 'Неверный ID задачи',
            'priority.required'     => 'Необходимо указать приоритет',
            'priority.in'           => 'Неверный приоритет',
            'title.required'        => 'Необходимо заполнить заголовок сообщения',
            'title.max'             => 'Длина заголовка не должна превышать 120 символов',
            'description.required'  => 'Необходимо заполнить описание сообщения',
            'description.max'       => 'Длина описания не должна превышать 2000 символовя',
        ];
    }
}
