<?php

namespace App\Http\Requests\Feedback;

use Illuminate\Foundation\Http\FormRequest;

class EditFeedback extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'feedbackId' => 'required|exists:feedback,id',
          'feedbackCategory' => 'required|exists:feedback_category,id',
          'feedbackStatus' => 'required|exists:feedback_status,id',
          'executingUserId' => 'exists:user,id',
        ];
    }

    public function messages()
    {
      return [
        'feedbackId.required' => 'Необходимо указать ID обращения',
        'feedbackId.exists' => 'Неверный ID обращения',
        'feedbackCategory.required' => 'Необходимо указать тип обращения',
        'feedbackCategory.exists' => 'Неверный ID типа обращения',
        'feedbackStatus.required' => 'Необходимо указать статус обращения',
        'feedbackStatus.exists' => 'Неверный ID статуса обращения',
        'executingUserId.exists' => 'Неверный ID пользователя',

      ];
    }
}
