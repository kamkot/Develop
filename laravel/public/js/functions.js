/**
 * Для автокомплита по пользователям
 *
 * @param suggestion
 * @param currentValue
 * @returns {string}
 */
var userFormatResult = function (suggestion, currentValue)
{
    var htmlSafeString = suggestion.value
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');

    var pattern = '(' + currentValue.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + ')';

    var tmp = htmlSafeString.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
    var company = suggestion.company;
    return (suggestion.ban == 1) ?
        '<span style="color: red;">' + tmp + '( Заблокирован ) </span>':
        tmp+' ( '+company+' ) ';
};