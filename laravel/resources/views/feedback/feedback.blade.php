@extends('layouts.app')
@section('feedback')
    <br/>
    {!! Breadcrumb::render() !!}
    <div class="row-fluid center">
        <div class="pull-left ">
            <a class="btn btn-info btn-sm" href="#createTask" data-toggle="modal"><i class="ico_sprite ico_plus"></i> Создать тикет</a>
        </div>
        <div class="center clearfix" style="margin-bottom: 5px;">
            {{ $feedback->appends(['taskView' => $taskView,'taskForDepartmentId'=> $taskForDepartmentId])->links('feedback.customPaginate') }}
        </div>
    </div>
    <div class="row-fluid" style="margin-bottom: 45px; !important;">
       @include('feedback.feedbackError')
        <div class="pull-left">
            <select class="form-control" id="taskForDepartment" name="taskForDepartmentId">
                @foreach($departmentList as $department)
                    @if($taskForDepartmentId == $department['id'])
                        <option value="{{ $department['id'] }}" selected="selected">{{ $department['name'] }}</option>
                    @else
                        <option value="{{ $department['id'] }}">{{ $department['name'] }}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="btn-group col-lg-3" data-toggle="buttons">
            @if($taskView == 'open')
                <label class="btn btn-default active">
                    <input type="radio" name="taskView" class="btn focus" value="open" checked>Открытые
                </label>
            @else
                <label class="btn btn-default">
                    <input type="radio" name="taskView" class="btn" value="open">Открытые
                </label>
            @endif
            @if($taskView == 'pause')
                <label class="btn btn-default active">
                    <input type="radio" name="taskView" class="btn focus" value="pause" checked>Приостановленные
                </label>
            @else
                <label class="btn btn-default">
                    <input type="radio" name="taskView" class="btn" value="pause">Приостановленные
                </label>
            @endif
            @if($taskView == 'close')
                <label class="btn btn-default active">
                    <input type="radio" name="taskView" class="btn focus" value="close" checked>Закрытые
                </label>
            @else
                <label class="btn btn-default">
                    <input type="radio" name="taskView" class="btn" value="close">Закрытые
                </label>
            @endif
        </div>

    </div>


    <table class="table table-bordered table-striped">
    <tr>
        <th>№</th>
        <th>Категория обращения</th>
        <th>Название обращения</th>
        <th>Создал</th>
        <th>Ответственный</th>
        <th>Статус</th>
        <th>Дата создания</th>
    </tr>
    @foreach ($feedback as $f)
        <tr style="background-color: {{ $f['category']['color'] }}">
            <td class="col-min">{{ $f['id'] }}</td>
            <td class="col-md-2">{{ $f['category']['description'] }}</td>
            <td class="col-md-2"><a href="{{ action('FeedbackController@viewFeedback',['id'=>$f['id']]) }}">{{ $f['description'] }}</a></td>
            <td class="col-md-2">{{ $f['author']['fullname']  }}</td>
            <td class="col-md-2">{{ $f['executingUser']['fullname'] }}</td>
            <td class="col-md-2">{{ $f['status']['description']  }}</td>
            <td class="col-md-2">{{ $f['open_date']  }}</td>
        </tr>
    @endforeach
</table>
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Заголовок модального окна -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Заголовок модального окна</h4>
      </div>
      <!-- Основное содержимое модального окна -->
      <div class="modal-body">
        Содержимое модального окна...
      </div>
      <!-- Футер модального окна -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary">Сохранить изменения</button>
      </div>
    </div>
  </div>
</div>
<div id="createTask" class="modal fade">
    <div class="modal-dialog">
        <form class="form-horizontal" name="createTask" id="createTaskForm" action="{{ action('FeedbackController@addFeedback') }}" method="post">
            <div class="modal-content" >
                <div class="modal-body">
                    <div class="form-group">
                        {!! csrf_field() !!}
                        <div class="col-lg-3">Отдел обращения:<sup><span style="color: red;">*</span></sup></div>
                        <div class="col-lg-9">
                            <select class="col-lg-12 form-control" id="departmentListModal" name="departmentId">
                                <option value="" selected="selected"></option>
                                @foreach($departmentList as $department)
                                    <option value="{{ $department['id'] }}">{{ $department['name'] }}</option>
                                @endforeach
                            </select>
                            <span id="errorDepartmentId" class="error alert-danger"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3">Тип обращения:<sup><span style="color: red;">*</span></sup></div>
                        <div class="col-lg-9">
                            <select class="col-lg-12 form-control" id="categoryList" name="categoryId">
                            </select>
                            <span id="errorCategoryId" class="error alert-danger"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3">Приоритет:<sup><span style="color: red;">*</span></sup></div>
                        <div class="col-lg-9">
                            <select class="col-lg-12 form-control" id="priority" name="priority">
                                <option value="low">Низкий</option>
                                <option value="middle">Обычный</option>
                                <option value="high">Высокий</option>
                            </select>
                            <span id="errorPriority" class="error alert-danger"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3">Заголовок:<sup><span style="color: red;">*</span></sup></div>
                        <div class="col-lg-9">
                            <input class="col-lg-12 input-large form-control" type="text" id="title" name="title" value="" />
                            <span id="errorTitle" class="error alert-danger"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-3 pull-left">Описание:<sup><span style="color: red;">*</span></sup></div>
                        <div class="col-lg-9">
                            <textarea class="col-lg-12 form-control" id="description" name="description" rows="4" style="resize: none;"></textarea>
                            <span id="errorDescription" class="error alert-danger"></span>
                        </div>
                    </div>
                    <p>
                        <sup><span style="color: red;">*</span></sup>
                        Поля обязательные для заполнения.<br />
                        Внимание, файлы можно прикрепить к задаче ПОСЛЕ ее создания.
                    </p>

                    <div class="modal-footer">
                        <button type="button" id="addTaskSubmit" class="btn btn-success">Отправить</button>
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Отменить</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function ()
    {
        $('#departmentListModal').on('change', function()
        {
            $.ajax(
            {
                url: './feedback/getCategoryListByDepartmentId/'+$(this).val(),
                type: 'get',
                dataType: 'json',
                success: function(response)
                {
                    var html = '<option value=""></option>';
                    for (var index in response)
                    {
                        html += '<option value="'+response[index]['id']+'" style="background:'+response[index]['color']+'">'+response[index]['description']+'</option>';
                    }
                    $('#categoryList').html(html);
                }
            });
        });

        $('#addTaskSubmit').on('click', function ()
        {
            var data = $('#createTaskForm').serialize();

            $.ajax(
            {
                url: './feedback/addFeedback',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function(response)
                {
                    if(response != '')
                    {
                        location.href = './feedback/viewFeedback?id='+response;
                    }
                },
                error: function (error) {
                    (error.responseJSON['departmentId']) ? $('#errorDepartmentId').html(error.responseJSON['departmentId']) : $('#errorDepartmentId').html('');
                    (error.responseJSON['categoryId']) ? $('#errorCategoryId').html(error.responseJSON['categoryId']) : $('#errorCategoryId').html('');
                    (error.responseJSON['priority']) ? $('#errorPriority').html(error.responseJSON['priority']) : $('#errorPriority').html('');
                    (error.responseJSON['title']) ? $('#errorTitle').html(error.responseJSON['title']) : $('#errorTitle').html('');
                    (error.responseJSON['description']) ? $('#errorDescription').html(error.responseJSON['description']) : $('#errorDescription').html('');
                }
            });
        });

        $('#taskForDepartment').on('change', function () {
            var taskView = $('input[name=taskView]:checked').val();
            var taskForDepartment = $('#taskForDepartment').val();
            location.href = './feedback?taskView='+taskView+'&taskForDepartmentId='+taskForDepartment;
        });

        $('input[name=taskView]').on('change', function () {
            var taskView = $('input[name=taskView]:checked').val();
            var taskForDepartment = $('#taskForDepartment').val();
            location.href = './feedback?taskView='+taskView+'&taskForDepartmentId='+taskForDepartment;
        });
    });
</script>
@endsection
