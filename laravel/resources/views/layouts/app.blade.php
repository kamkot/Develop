<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>MonCMS</title>

    <link rel="stylesheet" href="../../../laravel/public/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" href="../../../laravel/public/css/sprite.css" media="screen" />
    <link rel="stylesheet" href="../../../laravel/public/css/style.css" media="screen" />

    <script src="../../../laravel/public/js/jquery-3.1.1.min.js"></script>
    <script src="../../../laravel/public/js/bootstrap.min.js"></script>
    <script src="../../../laravel/public/js/jquery.autocomplete.1.2.18.js"></script>
    <script src="../../../laravel/public/js/functions.js"></script>
</head>

<body>
    @yield('menu')
<div class="container-fluid">
    @yield('content')
    @yield('feedback')
    @yield('viewFeedback')
</div>
</body>
</html>